# dory-dashboard v1.6.3 changelog

- ***new feature***: 
   1. custom step detail support tarFile and outputFiles
   2. custom step config page dockerVolumes and dockerEnvs webUI upgraded
   3. run page hide ABORT steps
